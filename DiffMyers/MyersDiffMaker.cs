﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using DiffAPI;

namespace DiffMyers
{
    /// <summary>
    /// Creates a diff using the Myers' algorithm.
    /// </summary>
    public class MyersDiffMaker : DiffMaker
    {
        /// <summary>
        /// Diff result. Null if no result has been computed yet.
        /// </summary>
        public ReadOnlyCollection<ChunkOriginInfo> Result { get; private set; }

        /// <summary>
        /// Shortest edit script length. Null when the length hasn't been computed yet.
        /// </summary>
        public int? ShortestEditScript { get; private set; }

        /// <summary>
        /// Myers' algorithm's D argument - how many diagonals should be considered at most.
        /// </summary>
        private int _maxDifference;

        /// <summary>
        /// Holds Myers' algorithm's D argument passed in the constructor to be used during _maxDifference assignment itself.
        /// </summary>
        private readonly int? _maxDifferenceTemp;

        /// <summary>
        /// Max allowable D.
        /// </summary>
        private readonly int? _maxDifferenceLimit;

        /// <summary>
        /// Creates a new instance.
        /// </summary>
        /// <param name="maxDifferenceLimit">Allows to set an upper limit for Myers' algorithm's D argument.</param>
        /// <param name="maxDifference">Allows to set the D argument manually. When null, the argument is set to be the sum of lengths of input sequences.</param>
        public MyersDiffMaker(int? maxDifferenceLimit = null, int? maxDifference = null)
        {
            _maxDifferenceLimit = maxDifferenceLimit;
            _maxDifferenceTemp = maxDifference;
        }

        /// <summary>
        /// Assigns input sequences and sets Myers' algorithm's D argument.
        /// </summary>
        /// <returns>Itself.</returns>
        public override DiffMaker AssignSequences(IChunk[] sequence1, IChunk[] sequence2)
        {
            _maxDifference = _maxDifferenceTemp ?? sequence1.Length + sequence2.Length;
            if (_maxDifferenceLimit < _maxDifference)
                _maxDifference = _maxDifferenceLimit.Value;
            return base.AssignSequences(sequence1, sequence2);
        }

        /// <summary>
        /// How many digits at most are required to display the order of a chunk.
        /// </summary>
        public override int ChunkNumberDigitsCount => Convert.ToInt32(Math.Ceiling(Math.Log10(Result.Count)));

        /// <summary>
        /// Runs the algorithm.
        /// </summary>
        /// <returns>Itself.</returns>
        public override DiffMaker Compute()
        {
            try
            {
                OnStatusUpdateInvoke(DiffStatus.Searching);
                ShortestEditScript = GetShortestEditScript(out var computingRecord);
                if (ShortestEditScript == -1) return null;

                OnStatusUpdateInvoke(DiffStatus.Backtracking);
                var stack = Backtrack(computingRecord);

                OnStatusUpdateInvoke(DiffStatus.Compiling);
                Result = new ReadOnlyCollection<ChunkOriginInfo>(stack.ToArray());

                OnStatusUpdateInvoke(DiffStatus.Done);
            }
            catch (DiffException e)
            {
                OnStatusUpdateInvoke(DiffStatus.Failed, e.Message);
            }

            return this;
        }

        /// <summary>
        /// Computes the length of the shortest edit script of input sequences given.
        /// </summary>
        /// <param name="computingRecord">Dynamic record of values for given diagonals computed in each iteration of the algorithm.</param>
        /// <returns>Length of the shortest edit script.</returns>
        /// <exception cref="DiffException">Thrown when the D argument is not sufficient to calculate the shortest edit script.</exception>
        private int GetShortestEditScript(out List<int?[]> computingRecord)
        {
            computingRecord = new List<int?[]>();

            int[] diagonalEndpoints;
            try
            {
                diagonalEndpoints = new int[_maxDifference * 2 + 1];
            }
            catch (OutOfMemoryException e)
            {
                throw new DiffException($"Algorithm launched on too great dimensions (D = {_maxDifference}). " +
                                        $"Use maxDifferenceLimit in the constructor to limit D. ({e})");
            }

            for (var eSLength = 0; eSLength <= _maxDifference; eSLength++) // how many edits were made
            {
                try
                {
                    computingRecord.Add(new int?[diagonalEndpoints.Length]);
                }
                catch (OutOfMemoryException e)
                {
                    throw new DiffException($"Algorithm launched on too great dimensions (D = {_maxDifference}). " +
                                            $"Use maxDifferenceLimit in the constructor to limit D. ({e})");
                }

                for (var diagonal = _maxDifference - eSLength;
                     diagonal <= _maxDifference + eSLength;
                     diagonal += 2) // which diagonal we're looking at
                {
                    int x;
                    if (diagonal == _maxDifference - eSLength || // looking at min diagonal
                        diagonal != _maxDifference + eSLength && // (not on max diagonal and got further on + diagonal)
                        diagonalEndpoints[diagonal - 1] < diagonalEndpoints[diagonal + 1])
                    {
                        // move down in the graph (take x from the diagonal above, calculate own y increased by 1)
                        x = diagonalEndpoints[diagonal + 1];
                    }
                    else
                    {
                        // move right in the graph (y stays the same - due to difference in diagonal indexing -, x needs to be increased)
                        x = diagonalEndpoints[diagonal - 1] + 1;
                    }

                    var y = GetY(x, diagonal);
                    while (x < Sequence1.Length && y < Sequence2.Length && Equals(Sequence1[x].Hash, Sequence2[y].Hash))
                    {
                        x++;
                        y++;
                    }

                    computingRecord[eSLength][diagonal] = diagonalEndpoints[diagonal] = x;

                    if (x < Sequence1.Length || y < Sequence2.Length) continue;

                    return eSLength;
                }
            }

            throw new DiffException($"Shortest edit script not found. Try launching the algorithm on greater " +
                                    $"dimensions (currently D = {_maxDifference}).");
        }

        /// <summary>
        /// Orders the input chunks (and determines their origin) by backtracking the output of the shortest edit script length algorithm. 
        /// </summary>
        /// <param name="computingRecord">Record created by the shortest edit script length algorithm.</param>
        /// <returns>Ordered chunks. The first chunk is on the top of the stack.</returns>
        /// <exception cref="DiffException">Thrown when computing record is unusable.</exception>
        private Stack<ChunkOriginInfo> Backtrack(List<int?[]> computingRecord)
        {
            var position = computingRecord.Count - 1;
            var diagonal = -1;
            for (var i = computingRecord[position].Length - 1; i >= 0; i--)
            {
                if (computingRecord[position][i] != Sequence1.Length) continue;
                diagonal = i;
                break;
            }

            if (diagonal == -1) throw new DiffException("Could not backtrack");

            var stack = new Stack<ChunkOriginInfo>();

            while (position >= 0)
            {
                var newPosition = position - 1;
                var newDiagonal = diagonal;
                if (newDiagonal - 1 < _maxDifference - newPosition || newDiagonal < _maxDifference + newPosition &&
                    computingRecord[newPosition][diagonal + 1] >
                    computingRecord[newPosition][diagonal - 1])
                    newDiagonal++;
                else newDiagonal--;

                if (computingRecord[position][diagonal] == null ||
                    newPosition >= 0 && computingRecord[newPosition][newDiagonal] == null)
                    throw new DiffException("Failed to backtrack");

                var x = (int) computingRecord[position][diagonal];
                var y = GetY(x, diagonal);
                // ReSharper disable once PossibleInvalidOperationException - taken care of in the if statement above
                var targetX = newPosition >= 0 ? computingRecord[newPosition][newDiagonal].Value : 0;
                if (newDiagonal < diagonal) targetX += 1;

                while (x > targetX)
                {
                    x--;
                    y--;
                    stack.Push(new ChunkOriginInfo(Sequence1[x], DiffOrigin.Moved));
                }

                if (x + y > 0)
                {
                    stack.Push(newDiagonal < diagonal
                        ? new ChunkOriginInfo(Sequence1[x - 1], DiffOrigin.Removed)
                        : new ChunkOriginInfo(Sequence2[y - 1], DiffOrigin.Added));
                }

                position = newPosition;
                diagonal = newDiagonal;
            }

            return stack;
        }

        /// <summary>
        /// Computes the Y-coordinate of a vertex.
        /// </summary>
        /// <param name="x">X-coordinate of the vertex.</param>
        /// <param name="diagonal">Diagonal on which the vertex is.</param>
        /// <returns>Y-coordinate of the vertex.</returns>
        private int GetY(int x, int diagonal) => x + _maxDifference - diagonal;
    }
}