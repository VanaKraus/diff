﻿namespace DiffAPI
{
    /// <summary>
    /// Holds information about a Chunk and its origin given by a diff computation.
    /// </summary>
    public class ChunkOriginInfo
    {
        public readonly IChunk Chunk;
        public readonly DiffOrigin Origin;

        public ChunkOriginInfo(IChunk chunk, DiffOrigin origin)
        {
            Origin = origin;
            Chunk = chunk;
        }
    }
}