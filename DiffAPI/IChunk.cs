﻿namespace DiffAPI
{
    /// <summary>
    /// Interface for chunks. Allows for its value used in comparisons to be different than its real value. 
    /// </summary>
    public interface IChunk
    {
        /// <summary>
        /// Chunk content to string.
        /// </summary>
        string ToString();
        /// <summary>
        /// Chunks real content.
        /// </summary>
        object Content { get; }
        /// <summary>
        /// Hash of the chunk. Used in comparisons.
        /// </summary>
        int Hash { get; }
    }
}