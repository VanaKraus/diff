﻿namespace DiffAPI
{
    /// <summary>
    /// Differentiation of files used in diff.
    /// </summary>
    public enum DiffFile
    {
        None,
        Original,
        New
    }
}