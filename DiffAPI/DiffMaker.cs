﻿namespace DiffAPI
{
    /// <summary>
    /// An abstract class for diff implementation.
    /// </summary>
    public abstract class DiffMaker
    {
        /// <summary>
        /// Original file sequence.
        /// </summary>
        protected IChunk[] Sequence1 { get; private set; }
        /// <summary>
        /// New file sequence.
        /// </summary>
        protected IChunk[] Sequence2 { get; private set; }

        /// <summary>
        /// Invoked when the algorithm reaches a new state.
        /// </summary>
        public event DiffStatusUpdate OnStatusUpdate;
        /// <summary>
        /// How many digits at most are required to display the order of a chunk.
        /// </summary>
        public abstract int ChunkNumberDigitsCount { get; }
        
        /// <summary>
        /// Assigns input sequences for the diff algorithm.
        /// </summary>
        /// <param name="sequence1">First sequence.</param>
        /// <param name="sequence2">Second sequence.</param>
        /// <returns>Itself.</returns>
        public virtual DiffMaker AssignSequences(IChunk[] sequence1, IChunk[] sequence2)
        {
            Sequence1 = sequence1;
            Sequence2 = sequence2;
            return this;
        }

        /// <summary>
        /// Runs the diff algorithm.
        /// </summary>
        /// <returns>Itself.</returns>
        public abstract DiffMaker Compute();

        /// <summary>
        /// Invokes OnStatusUpdate.
        /// </summary>
        protected void OnStatusUpdateInvoke(DiffStatus status, string message = null)
        {
            OnStatusUpdate?.Invoke(status, message);
        }
    }
}