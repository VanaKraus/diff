﻿namespace DiffAPI
{
    /// <summary>
    /// Describes types of split.
    ///
    /// Inclusive: include separator within the previous chunk's value and hash.
    /// Regular: include separator within the previous chunk's value but not within its hash.
    /// Standalone: treat separator as a standalone chunk.  
    /// </summary>
    public enum SplitType
    {
        Regular,
        Inclusive,
        Standalone
    }
}