﻿namespace DiffAPI
{
    /// <summary>
    /// Describes the origin of a chunk, i.e. what happens to it in the given diff.
    /// </summary>
    public enum DiffOrigin
    {
        None,
        Moved,
        Removed,
        Added
    }
}