﻿namespace DiffAPI
{
    /// <summary>
    /// Diff statuses for diff algorithms.
    /// </summary>
    public enum DiffStatus { None, Loading, Preparing, Searching, Backtracking, Matching, Correcting, Compiling, Done, Failed }
}