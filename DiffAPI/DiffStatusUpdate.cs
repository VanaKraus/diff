﻿namespace DiffAPI
{
    public delegate void DiffStatusUpdate(DiffStatus status, string message = "");
}