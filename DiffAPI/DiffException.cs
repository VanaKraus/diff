﻿using System;

namespace DiffAPI
{
    public class DiffException : Exception
    {
        public DiffException(string message) : base(message) {}
    }
}