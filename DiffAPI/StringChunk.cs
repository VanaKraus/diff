﻿using System.Text;

namespace DiffAPI
{
    /// <summary>
    /// Chunk to contain strings.
    /// </summary>
    public class StringChunk : IChunk
    {
        /// <summary>
        /// Hash. Determined from Radix.
        /// </summary>
        public int Hash => Radix.GetHashCode();

        /// <summary>
        /// Content of the chunk.
        /// </summary>
        public object Content => ToString();

        /// <summary>
        /// Main content of the chunk. Used in comparisons.
        /// </summary>
        public string Radix { get; }

        /// <summary>
        /// Content inserted in front of the radix. Not used in comparisons.
        /// </summary>
        public string Prefix { get; private set; }

        /// <summary>
        /// Content appended after the radix. Not used in comparisons.
        /// </summary>
        public string Suffix { get; private set; }

        /// <summary>
        /// Creates a new instance.
        /// </summary>
        public StringChunk(string radix, string prefix = null, string suffix = null)
        {
            Radix = radix;
            Prefix = prefix;
            Suffix = suffix;
        }

        /// <summary>
        /// Inserts a new string to the front of the prefix.
        /// </summary>
        public void InsertBeforePrefix(string s) => Prefix = s + Prefix;

        /// <summary>
        /// Appends a new string to the end of the suffix.
        /// </summary>
        public void AppendAfterSuffix(string s) => Suffix += s;

        public override string ToString()
        {
            var sb = new StringBuilder();
            if (Prefix != null) sb.Append(Prefix);
            sb.Append(Radix);
            if (Suffix != null) sb.Append(Suffix);
            return sb.ToString();
        }
    }
}