﻿using System.Text.RegularExpressions;

namespace DiffAPI
{
    /// <summary>
    /// A rule for text splitting.
    /// </summary>
    public class SplitRule
    {
        /// <summary>
        /// What to do with separators.
        /// </summary>
        public readonly SplitType SplitType;

        /// <summary>
        /// Regex to find separators.
        /// </summary>
        public readonly Regex Regex;

        /// <summary>
        /// Creates a new instance.
        /// </summary>
        /// <param name="splitType">SplitType.</param>
        /// <param name="regex">Regex.</param>
        public SplitRule(SplitType splitType, Regex regex)
        {
            SplitType = splitType;
            Regex = regex;
        }

        /// <summary>
        /// Creates a new instance.
        /// </summary>
        /// <param name="splitType">SplitType.</param>
        /// <param name="regexPattern">Regex expression. Gets converted into a Regex object.</param>
        public SplitRule(SplitType splitType, string regexPattern) : this(splitType, new Regex(regexPattern))
        {
        }
    }
}