﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace DiffAPI
{
    /// <summary>
    /// Used to turn a string into chunks. 
    /// </summary>
    public class TextSplitter
    {
        /// <summary>
        /// Pre-defined instance splitting the input on new lines, which are included as suffixes.
        /// </summary>
        public static TextSplitter NewLine => new TextSplitter(new SplitRule(SplitType.Regular, @"\r?\n"));

        /// <summary>
        /// Pre-defined instance splitting the input into code-morphemes-like chunks.
        /// </summary>
        public static TextSplitter Code => new TextSplitter(new SplitRule(SplitType.Regular, @"\r?\n"), // new lines
            new SplitRule(SplitType.Regular, @"\s+"), // other white spaces
            new SplitRule(SplitType.Standalone, @"[\[\{\(\)\}\]\""\']+"), // brackets
            new SplitRule(SplitType.Standalone, @"[\*\-\+\/\<\>\=\!\?\|\&\^]+"), // operators
            new SplitRule(SplitType.Standalone, @"[\$\@\.\,\;\:]") // punctuation
        );

        /// <summary>
        /// Pre-defined instance splitting the input on new lines, which are included within radixes.
        /// </summary>
        public static TextSplitter SimpleLine => new TextSplitter(new SplitRule(SplitType.Inclusive, @"\r?\n"));

        /// <summary>
        /// Pre-defined instance splitting the input on white spaces, which are included within radixes.
        /// </summary>
        public static TextSplitter SimpleWhitespace => new TextSplitter(new SplitRule(SplitType.Inclusive, @"\s+"));

        /// <summary>
        /// Split rules. 
        /// </summary>
        public readonly SplitRule[] SplitRules;

        /// <summary>
        /// Creates a new instance.
        /// </summary>
        public TextSplitter(params SplitRule[] splitRules)
        {
            SplitRules = splitRules ?? Array.Empty<SplitRule>();
        }

        /// <summary>
        /// Splits a string into StringChunks using defined SplitRules.
        /// </summary>
        /// <param name="s">String to be split.</param>
        /// <returns>Resulting StringChunks.</returns>
        public List<StringChunk> Split(string s) => Split(new StringChunk(s));

        /// <summary>
        /// Splits StringChunks into further StringChunks using defined SplitRules.
        /// </summary>
        /// <param name="chunks">Chunks to be split.</param>
        /// <returns>Resulting StringChunks.</returns>
        public List<StringChunk> Split(params StringChunk[] chunks)
        {
            var res = new List<StringChunk>(chunks);
            foreach (var splitRule in SplitRules)
            {
                for (var i = 0; i < res.Count; i++)
                {
                    var newChunks = Split(res[i], splitRule);
                    if (newChunks.Count <= 1) continue;

                    res.RemoveAt(i);
                    res.InsertRange(i, newChunks);
                    i += newChunks.Count - 1;
                }
            }

            return res;
        }

        /// <summary>
        /// Splits a StringChunk's radix using a given SplitRule.
        /// </summary>
        /// <returns>Resulting StringChunks.</returns>
        public static List<StringChunk> Split(StringChunk chunk, SplitRule rule)
        {
            var chunks = Split(chunk.Radix, rule);
            chunks[0].InsertBeforePrefix(chunk.Prefix);
            chunks[chunks.Count - 1].AppendAfterSuffix(chunk.Suffix);
            return chunks;
        }

        /// <summary>
        /// Splits a string using a given SplitRule.
        /// </summary>
        /// <returns>Resulting StringChunks.</returns>
        public static List<StringChunk> Split(string s, SplitRule rule)
        {
            var chunks = new List<StringChunk>();
            var matches = rule.Regex.Matches(s);
            var i = 0;

            foreach (Match match in matches)
            {
                switch (rule.SplitType)
                {
                    case SplitType.Inclusive:
                        var ei = match.Index + match.Length;
                        chunks.Add(new StringChunk(s.Substring(i, ei - i)));
                        i = ei;
                        break;
                    case SplitType.Regular:
                        chunks.Add(new StringChunk(s.Substring(i, match.Index - i), suffix: match.Value));
                        i = match.Index + match.Length;
                        break;
                    case SplitType.Standalone:
                        chunks.Add(new StringChunk(s.Substring(i, match.Index - i)));
                        chunks.Add(new StringChunk(match.Value));
                        i = match.Index + match.Length;
                        break;
                }
            }

            chunks.Add(new StringChunk(s.Substring(i)));
            return chunks;
        }
    }
}