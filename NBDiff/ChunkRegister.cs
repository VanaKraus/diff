﻿using System.Collections.Generic;
using DiffAPI;

namespace NBDiff
{
    /// <summary>
    /// Element of an alphabet of different chunks. Holds references to all chunks of its value.
    /// </summary>
    public class ChunkRegister
    {
        /// <summary>
        /// Hash of the element.
        /// </summary>
        public readonly int ChunkHash;

        /// <summary>
        /// Usages from the original file.
        /// </summary>
        public List<ChunkUsage> F1Usages { get; private set; }

        /// <summary>
        /// Usages from the new file.
        /// </summary>
        public List<ChunkUsage> F2Usages { get; private set; }

        /// <summary>
        /// Creates a new instance.
        /// </summary>
        /// <param name="chunkHash">Hash of the described element.</param>
        public ChunkRegister(int chunkHash)
        {
            F1Usages = new List<ChunkUsage>();
            F2Usages = new List<ChunkUsage>();

            ChunkHash = chunkHash;
        }

        /// <summary>
        /// Finds the appropriate list of usages from given DiffFile.
        /// </summary>
        public List<ChunkUsage> Usages(DiffFile diffFile)
        {
            switch (diffFile)
            {
                case DiffFile.Original:
                    return F1Usages;
                case DiffFile.New:
                    return F2Usages;
                case DiffFile.None:
                default:
                    return null;
            }
        }

        /// <summary>
        /// Registers a new chunk and creates its ChunkUsage.
        /// </summary>
        /// <param name="diffFile">File from which the chunk comes.</param>
        /// <param name="chunk">The chunk.</param>
        /// <param name="chunkIndex">Order of the chunk in its input sequence.</param>
        /// <exception cref="DiffException">Thrown when the chunk doesn't belong to this ChunkRegister.</exception>
        /// <returns>ChunkUsage of the chunk.</returns>
        public ChunkUsage NewChunkUsage(DiffFile diffFile, IChunk chunk, int chunkIndex)
        {
            if (chunk.Hash != ChunkHash) throw new DiffException("Tried to register an unfitting chunk.");
            var chunkUsage = new ChunkUsage(chunk, chunkIndex, this);
            Usages(diffFile).Add(chunkUsage);
            return chunkUsage;
        }

        /// <summary>
        /// Matches the chunks across both input files naively.
        /// </summary>
        /// <param name="includeIncomplete">If true, ChunkMatch objects are created even from chunks for which no matching counterpart has been left.</param>
        /// <returns>Newly created ChunkMatches.</returns>
        public ChunkMatch[] NaiveChunkMatch(bool includeIncomplete = false)
        {
            int i1 = 0, i2 = 0;
            var matches = new List<ChunkMatch>();
            while (i1 < F1Usages.Count && i2 < F2Usages.Count)
            {
                if (F1Usages[i1].Match != null)
                {
                    i1++;
                    continue;
                }

                if (F2Usages[i2].Match != null)
                {
                    i2++;
                    continue;
                }

                matches.Add(ChunkMatch.RegisterMatch(F1Usages[i1], F2Usages[i2]));

                i1++;
                i2++;
            }

            if (!includeIncomplete) return matches.ToArray();

            for (; i1 < F1Usages.Count; i1++)
                matches.Add(ChunkMatch.RegisterMatch(F1Usages[i1], null));
            for (; i2 < F2Usages.Count; i2++)
                matches.Add(ChunkMatch.RegisterMatch(null, F2Usages[i2]));

            return matches.ToArray();
        }

        protected bool Equals(ChunkRegister other)
        {
            return ChunkHash == other.ChunkHash;
        }

        public override int GetHashCode()
        {
            return ChunkHash;
        }
    }
}