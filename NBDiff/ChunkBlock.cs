﻿using System.Collections.Generic;
using DiffAPI;

namespace NBDiff
{
    /// <summary>
    /// Block of consecutive chunks with the same origin.
    /// </summary>
    public class ChunkBlock
    {
        /// <summary>
        /// Origin of the chunks.
        /// </summary>
        public DiffOrigin Origin { get; }

        /// <summary>
        /// By how many lines the chunks were moved. Null when Origin is different than DiffOrigin.Moved.
        /// </summary>
        public int? Diff { get; }

        /// <summary>
        /// ChunkMatches holding the chunks themselves.
        /// </summary>
        private readonly List<ChunkMatch> _chunkMatches;

        /// <summary>
        /// ChunkMatches holding the chunks themselves.
        /// </summary>
        public ChunkMatch[] ChunkMatches => _chunkMatches.ToArray();

        /// <summary>
        /// Initializes the block.
        /// </summary>
        /// <param name="chunkMatches">ChunkMatches of the block.</param>
        /// <exception cref="DiffException">Thrown when chunkMatches aren't consistent (consecutive or of the same origin).</exception>
        public ChunkBlock(List<ChunkMatch> chunkMatches)
        {
            _chunkMatches = chunkMatches;
            Diff = chunkMatches.Count > 0 ? chunkMatches[0].Diff : null;
            Origin = chunkMatches.Count > 0 ? chunkMatches[0].Origin : DiffOrigin.None;

            foreach (var match in chunkMatches)
            {
                match.Block = this;
                if (match.Origin != Origin || match.Diff != Diff) throw new DiffException("Inconsistent ChunkMatches.");
            }
        }
    }
}