﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using DiffAPI;

namespace NBDiff
{
    /// <summary>
    /// Creates a diff using the NBDiff algorithm.
    /// </summary>
    public class NbDiffMaker : DiffMaker
    {
        /// <summary>
        /// All resulting blocks.
        /// </summary>
        public ReadOnlyCollection<ChunkBlock> BlocksAll { get; private set; }

        /// <summary>
        /// Resulting blocks relevant to each file.
        /// </summary>
        public ReadOnlyDictionary<DiffFile, ReadOnlyCollection<ChunkBlock>> FileBlocks { get; private set; }

        /// <summary>
        /// Resulting file usages relevant to each file.
        /// </summary>
        public ReadOnlyDictionary<DiffFile, ReadOnlyCollection<ChunkUsage>> FileChunkUsages { get; private set; }

        /// <summary>
        /// Selects length of the longest supplied input sequence.
        /// </summary>
        public int ChunkUsagesCount => FileChunkUsages.Values.Select(value => value.Count).Max();

        /// <summary>
        /// How many digits at most are required to display the order of a chunk.
        /// </summary>
        public override int ChunkNumberDigitsCount => Convert.ToInt32(Math.Ceiling(Math.Log10(ChunkUsagesCount)));

        /// <summary>
        /// Chunk registers accessed via their hash.
        /// </summary>
        private Dictionary<int, ChunkRegister> _chunkRegisters;

        /// <summary>
        /// ChunkMatches.
        /// </summary>
        private List<ChunkMatch> _matches;

        /// <summary>
        /// ChunkUsages of the original file.
        /// </summary>
        private ChunkUsage[] _chunkUsages1;

        /// <summary>
        /// ChunkUsages of the new file.
        /// </summary>
        private ChunkUsage[] _chunkUsages2;

        /// <summary>
        /// Runs the algorithm.
        /// </summary>
        /// <returns>Itself.</returns>
        public override DiffMaker Compute()
        {
            OnStatusUpdateInvoke(DiffStatus.Matching);
            _chunkRegisters =
                CreateChunkRegisters(Sequence1, Sequence2, out _chunkUsages1, out _chunkUsages2);
            _matches = CreateMatches(_chunkRegisters, _chunkUsages1, _chunkUsages2);

            // block corrections
            OnStatusUpdateInvoke(DiffStatus.Correcting);
            _matches = SealHoles(_matches, _chunkUsages1);
            var blocks = CreateBlocks(_matches);

            OnStatusUpdateInvoke(DiffStatus.Compiling);
            BlocksAll = Array.AsReadOnly(blocks.Where(block => block.Origin != DiffOrigin.None).ToArray());

            FileBlocks = new ReadOnlyDictionary<DiffFile, ReadOnlyCollection<ChunkBlock>>(
                new Dictionary<DiffFile, ReadOnlyCollection<ChunkBlock>>
                {
                    {DiffFile.Original, Array.AsReadOnly(ChunkBlocksFromChunkUsages(_chunkUsages1))},
                    {DiffFile.New, Array.AsReadOnly(ChunkBlocksFromChunkUsages(_chunkUsages2))},
                });

            FileChunkUsages = new ReadOnlyDictionary<DiffFile, ReadOnlyCollection<ChunkUsage>>(
                new Dictionary<DiffFile, ReadOnlyCollection<ChunkUsage>>
                {
                    {DiffFile.Original, Array.AsReadOnly(_chunkUsages1)},
                    {DiffFile.New, Array.AsReadOnly(_chunkUsages2)}
                });

            OnStatusUpdateInvoke(DiffStatus.Done);
            return this;
        }

        /// <summary>
        /// Creates ChunkUsages and ChunkRegisters of chunks from both input files.
        /// </summary>
        /// <param name="file1Chunks">Chunks from the original file.</param>
        /// <param name="file2Chunks">Chunks from the new file.</param>
        /// <param name="file1ChunkUsages">Newly-created ChunkUsages of the original file.</param>
        /// <param name="file2ChunkUsages">Newly-created ChunkUsages of the new file.</param>
        /// <returns>Newly-created ChunkRegisters.</returns>
        private static Dictionary<int, ChunkRegister> CreateChunkRegisters(IChunk[] file1Chunks, IChunk[] file2Chunks,
            out ChunkUsage[] file1ChunkUsages, out ChunkUsage[] file2ChunkUsages)
        {
            var registers = new Dictionary<int, ChunkRegister>();

            file1ChunkUsages = RegisterChunks(registers, file1Chunks, DiffFile.Original);
            file2ChunkUsages = RegisterChunks(registers, file2Chunks, DiffFile.New);

            return registers;
        }

        /// <summary>
        /// Creates ChunkUsages of chunks from a given file and assigns them to their appropriate register.
        /// </summary>
        /// <param name="registers">A dictionary of registers.</param>
        /// <param name="chunks">Chunks to be registered.</param>
        /// <param name="diffFile">File from which the chunks come.</param>
        /// <returns>Newly-created ChunkUsages.</returns>
        private static ChunkUsage[] RegisterChunks(Dictionary<int, ChunkRegister> registers, IChunk[] chunks,
            DiffFile diffFile)
        {
            var usages = new ChunkUsage[chunks.Length];

            for (var i = 0; i < chunks.Length; i++)
            {
                var chunk = chunks[i];
                var hash = chunk.Hash;
                ChunkRegister register;

                if (!registers.ContainsKey(hash))
                {
                    register = new ChunkRegister(chunk.Hash);
                    registers.Add(hash, register);
                }
                else
                    register = registers[hash];

                usages[i] = register.NewChunkUsage(diffFile, chunk, i);
            }

            return usages;
        }

        /// <summary>
        /// Naively creates matches between the two input files using registers already computed.
        /// </summary>
        /// <param name="chunkRegisters">Already computed ChunkRegisters.</param>
        /// <param name="file1ChunkUsages">ChunkUsages of the original file.</param>
        /// <param name="file2ChunkUsages">ChunkUsages of the new file.</param>
        /// <returns></returns>
        private static List<ChunkMatch> CreateMatches(Dictionary<int, ChunkRegister> chunkRegisters,
            ChunkUsage[] file1ChunkUsages, ChunkUsage[] file2ChunkUsages)
        {
            var matches = new List<ChunkMatch>();

            // Use a naive chunk match in every register
            foreach (var register in chunkRegisters.Values)
            {
                register.NaiveChunkMatch(true);
            }

            // Go through all chunks removed from the beginning
            foreach (var lu in file1ChunkUsages)
            {
                if (lu.Match.Usage[DiffFile.New] != null) break;
                matches.Add(lu.Match);
            }

            // Go through all chunks from the new file
            foreach (var chunkUsage2 in file2ChunkUsages)
            {
                matches.Add(chunkUsage2.Match);

                var chunkUsage1 = chunkUsage2.Match.Usage[DiffFile.Original];
                if (chunkUsage1 == null) continue;

                // Add all removed chunks directly following the current chunkUsage2
                while (chunkUsage1.ChunkIndex < file1ChunkUsages.Length - 1)
                {
                    chunkUsage1 = file1ChunkUsages[chunkUsage1.ChunkIndex + 1];
                    if (chunkUsage1.Match.Origin != DiffOrigin.Removed) break;
                    matches.Add(chunkUsage1.Match);
                }
            }

            return matches;
        }

        /// <summary>
        /// Attempts to find and seal "holes" caused by naive chunk-matching.
        /// </summary>
        /// <param name="matches">All matches.</param>
        /// <param name="file1ChunkUsages">ChunkUsages of the original file.</param>
        /// <returns>Corrected list of matches.</returns>
        private static List<ChunkMatch> SealHoles(List<ChunkMatch> matches, ChunkUsage[] file1ChunkUsages)
        {
            matches = new List<ChunkMatch>(matches);
            int? diff = null; // difference between the old and the new chunk index
            for (var i = 0; i < matches.Count; i++)
            {
                if (matches[i].Origin == DiffOrigin.Removed) continue; // nothing to be fixed in the new file

                // attempts to seal "holes" caused by naive chunk-matching (e.g. a misplaced origin of a blank chunk)
                if (diff != null && diff.Value != matches[i].Diff)
                    SealBlockHole(matches, i, diff.Value, file1ChunkUsages);

                diff = matches[i].Diff;
            }

            return matches;
        }

        /// <summary>
        /// Seals a hole caused by naive chunk-matching by swapping relations between two pairs of chunks (turning matches S1-T1, S2-T2 into S2-T1, S1-T2). 
        /// </summary>
        /// <param name="matches">A list of all matches.</param>
        /// <param name="matchIndex">Position of the hole-causing match.</param>
        /// <param name="previousDiff">Line difference which the match should have in order to not create a hole.</param>
        /// <param name="file1ChunkUsages">ChunkUsages of the original file.</param>
        private static void SealBlockHole(List<ChunkMatch> matches, int matchIndex, int previousDiff,
            ChunkUsage[] file1ChunkUsages)
        {
            var match = matches[matchIndex];

            var cu1 = match.Usage[DiffFile.Original];
            var cu2 = match.Usage[DiffFile.New];

            if (cu2 == null) return;

            // tries to find a chunk (parallelCu1) from the old file which should have been matched with cu2
            // provided that it belongs to the same block (as cu2)
            var parallelCu1Index = cu2.ChunkIndex - previousDiff;

            if (parallelCu1Index < 0 || parallelCu1Index >= file1ChunkUsages.Length) // if out of range
                return;

            var parallelCu1 = file1ChunkUsages[parallelCu1Index];

            // checks if parallelLu1 really could belong to the same block as lu2
            if (!parallelCu1.Register.Equals(match.ChunkRegister) || parallelCu1.Match.Equals(match)) return;

            // destroy old matches, create new ones
            var altCu2 = parallelCu1.Match.Usage[DiffFile.New];
            var altCu2MatchIndex = matches.IndexOf(parallelCu1.Match);

            parallelCu1.Match.Destroy();
            match.Destroy();

            var newDesiredMatch = ChunkMatch.RegisterMatch(parallelCu1, cu2);
            var newAltMatch = ChunkMatch.RegisterMatch(cu1, altCu2);

            if (matchIndex < altCu2MatchIndex) // current match came first
            {
                matches.RemoveAt(altCu2MatchIndex);
                matches.RemoveAt(matchIndex);

                matches.Insert(matchIndex, newDesiredMatch);
                matches.Insert(altCu2MatchIndex, newAltMatch);
            }
            else // alternative match came first
            {
                matches.RemoveAt(matchIndex);
                matches.RemoveAt(altCu2MatchIndex);

                matches.Insert(altCu2MatchIndex, newAltMatch);
                matches.Insert(matchIndex, newDesiredMatch);
            }
        }

        /// <summary>
        /// Turns similar consecutive matches into blocks.
        /// </summary>
        /// <returns>Newly-created blocks.</returns>
        private static IEnumerable<ChunkBlock> CreateBlocks(List<ChunkMatch> matches)
        {
            var blocks = new List<ChunkBlock>();
            var currentBlockMatches = new List<ChunkMatch>();

            int? diff = null; // difference between the old and the new chunk index
            for (var i = 0; i < matches.Count; i++)
            {
                var match = matches[i];

                if (match.Diff != diff || (i > 0 && matches[i].Origin != matches[i - 1].Origin))
                {
                    blocks.Add(new ChunkBlock(currentBlockMatches));
                    currentBlockMatches = new List<ChunkMatch>();
                }

                currentBlockMatches.Add(matches[i]);
                diff = match.Diff;
            }

            blocks.Add(new ChunkBlock(currentBlockMatches));

            return blocks;
        }

        /// <summary>
        /// Returns blocks of consecutive ChunkUsages.
        /// </summary>
        /// <returns>Blocks found.</returns>
        /// <exception cref="DiffException">Thrown when a ChunkUsage not belonging to any block is found.</exception>
        private static ChunkBlock[] ChunkBlocksFromChunkUsages(IEnumerable<ChunkUsage> chunkUsages)
        {
            var chunkBlocks = new List<ChunkBlock>();
            foreach (var usage in chunkUsages)
            {
                if (usage.Match?.Block == null)
                    throw new DiffException("Chunk usage not matched or not present in a block");

                var block = usage.Match.Block;
                if (chunkBlocks.Count == 0 || !block.Equals(chunkBlocks[chunkBlocks.Count - 1]))
                    chunkBlocks.Add(block);
            }

            return chunkBlocks.ToArray();
        }
    }
}