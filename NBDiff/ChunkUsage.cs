﻿using DiffAPI;

namespace NBDiff
{
    /// <summary>
    /// Describes the appearance of a chunk within an input sequence.
    /// </summary>
    public class ChunkUsage
    {
        /// <summary>
        /// Position of the chunk.
        /// </summary>
        public readonly int ChunkIndex;

        /// <summary>
        /// The chunk itself.
        /// </summary>
        public readonly IChunk Chunk;

        /// <summary>
        /// ChunkRegister of the chunk.
        /// </summary>
        public readonly ChunkRegister Register;

        /// <summary>
        /// Match of the chunk.
        /// </summary>
        public ChunkMatch Match { get; set; }

        /// <summary>
        /// Creates a new instance.
        /// </summary>
        /// <param name="chunk">The chunk.</param>
        /// <param name="chunkIndex">Position of the chunk.</param>
        /// <param name="register">ChunkRegister of the chunk.</param>
        public ChunkUsage(IChunk chunk, int chunkIndex, ChunkRegister register)
        {
            ChunkIndex = chunkIndex;
            Register = register;
            Chunk = chunk;
        }
    }
}