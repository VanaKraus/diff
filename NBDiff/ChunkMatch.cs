﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using DiffAPI;

namespace NBDiff
{
    /// <summary>
    /// A pair of the same chunks coming from different files.
    /// </summary>
    public class ChunkMatch
    {
        /// <summary>
        /// Match's affiliation to an element of the alphabet.
        /// </summary>
        public readonly ChunkRegister ChunkRegister;

        /// <summary>
        /// Chunks present in this match.
        /// </summary>
        public readonly ReadOnlyDictionary<DiffFile, ChunkUsage> Usage;

        /// <summary>
        /// Type of change described by this match.
        /// </summary>
        public readonly DiffOrigin Origin;

        /// <summary>
        /// By how many lines the chunk has been moved between the input files. Null when Origin is anything other than DiffOrigin.Moved.
        /// </summary>
        public readonly int? Diff;

        /// <summary>
        /// Affiliation to a ChunkBlock.
        /// </summary>
        public ChunkBlock Block
        {
            get => _block;
            set
            {
                if (_block == null) _block = value;
            }
        }

        /// <summary>
        /// Affiliation to a ChunkBlock.
        /// </summary>
        private ChunkBlock _block;

        /// <summary>
        /// When true, no chunks are present in this match.
        /// </summary>
        public bool Empty => Usage == null || Usage.Count == 0;

        /// <summary>
        /// Creates a new match.
        /// </summary>
        /// <param name="usage1">ChunkUsage from the first file.</param>
        /// <param name="diffFile1">First file specification.</param>
        /// <param name="usage2">ChunkUsage from the second file.</param>
        /// <param name="diffFile2">Second file specification.</param>
        /// <exception cref="DiffException">Thrown when chunks don't belong to the same register or when one of the chunks is already matched.</exception>
        public ChunkMatch(ChunkUsage usage1, DiffFile diffFile1, ChunkUsage usage2,
            DiffFile diffFile2)
        {
            if (usage1 != null && usage2 != null)
            {
                if (!usage1.Register.Equals(usage2.Register))
                    throw new DiffException("Cannot match different chunks");
                if (usage1.Match != null && !usage1.Match.Empty || usage2.Match != null && !usage2.Match.Empty)
                    throw new DiffException("Chunks already matched");
            }

            usage1?.Match?.Destroy();
            usage2?.Match?.Destroy();

            ChunkRegister = usage1?.Register ?? usage2?.Register;
            Usage = new ReadOnlyDictionary<DiffFile, ChunkUsage>(new Dictionary<DiffFile, ChunkUsage>
            {
                {diffFile1, usage1}, {diffFile2, usage2}
            });

            if (usage1 != null) usage1.Match = this;
            if (usage2 != null) usage2.Match = this;

            if (usage1 != null && usage2 != null) Origin = DiffOrigin.Moved;
            else if (usage1 == null && usage2 != null) Origin = DiffOrigin.Added;
            else if (usage1 != null) Origin = DiffOrigin.Removed;
            else Origin = DiffOrigin.None;

            Diff = usage2?.ChunkIndex - usage1?.ChunkIndex;
        }
        
        /// <summary>
        /// Creates a new match.
        /// </summary>
        /// <param name="chunkUsage1">Chunk usage from the original file.</param>
        /// <param name="chunkUsage2">Chunk usage from the new file.</param>
        /// <returns>A new chunk match.</returns>
        public static ChunkMatch RegisterMatch(ChunkUsage chunkUsage1, ChunkUsage chunkUsage2)
        {
            return new ChunkMatch(chunkUsage1, DiffFile.Original, chunkUsage2, DiffFile.New);
        }

        /// <summary>
        /// Destroys itself and references to itself.
        /// </summary>
        public void Destroy()
        {
            foreach (var chunkUsage in Usage.Values)
                if (chunkUsage?.Match != null && chunkUsage.Match.Equals(this))
                    chunkUsage.Match = null;
        }

        ~ChunkMatch()
        {
            Destroy();
        }
    }
}