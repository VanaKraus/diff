﻿using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using diff_main.Properties;
using DiffAPI;
using DiffMyers;
using NBDiff;

namespace diff_main
{
    /// <summary>
    /// Summarizes all controls from MainWindow related to a certain file.
    /// </summary>
    internal class FileControlsSet
    {
        /// <summary>
        /// Button used to select a new file.
        /// </summary>
        private readonly Button _buttonOpenFile;

        /// <summary>
        /// RichTextBox to display file contents into.
        /// </summary>
        private readonly RichTextBox _textBoxFileContent;

        /// <summary>
        /// Label to display the file name.
        /// </summary>
        private readonly Label _labelFileName;

        /// <summary>
        /// Diff characteristic of files handled by this instance of FileControlsSet.
        /// </summary>
        private readonly DiffFile _diffFileAffiliation;

        /// <summary>
        /// Instance of MainWindow.
        /// </summary>
        private readonly MainWindow _mainWindow;

        /// <summary>
        /// Path to the currently selected file.
        /// </summary>
        public string SelectedFile
        {
            get => _selectedFile;
            set
            {
                _selectedFile = value;
                _labelFileName.Content = Path.GetFileName(_selectedFile);
            }
        }

        /// <summary>
        /// Holds the value of SelectedFile property.
        /// </summary>
        private string _selectedFile;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="diffFileAffiliation">Diff characteristic of files handled by this instance of FileControlsSet.</param>
        /// <param name="mainWindow">Instance of MainWindow.</param>
        /// <param name="buttonOpenFile">Button used to select a new file.</param>
        /// <param name="textBoxFileContent">RichTextBox to display file contents into.</param>
        /// <param name="labelFileName">Label to display the file name.</param>
        public FileControlsSet(DiffFile diffFileAffiliation, MainWindow mainWindow, Button buttonOpenFile,
            RichTextBox textBoxFileContent, Label labelFileName)
        {
            _buttonOpenFile = buttonOpenFile;
            _textBoxFileContent = textBoxFileContent;
            _labelFileName = labelFileName;
            _mainWindow = mainWindow;
            _diffFileAffiliation = diffFileAffiliation;

            mainWindow.OnDiffUpdate += DiffUpdate;
            mainWindow.OnSettingsUpdate += LayoutUpdate;
            
            LayoutUpdate();
        }

        /// <summary>
        /// Updates layout properties.
        /// </summary>
        private void LayoutUpdate()
        {
            _textBoxFileContent.FontSize = Settings.Default.Setting_FontSize;
        }

        /// <summary>
        /// Displays the file contents into _textBoxFileContent as plain text.
        /// </summary>
        public void SimpleDisplay()
        {
            if (!File.Exists(SelectedFile)) return;
            _textBoxFileContent.Document.Blocks.Clear();

            var text = File.ReadAllText(SelectedFile);

            _textBoxFileContent.Document.PageWidth = PageWidth(text.Length, text.Count(c => c == '\n'));
            _textBoxFileContent.AppendText(text);
        }

        /// <summary>
        /// Listener to MainWindow.OnDiffUpdate. Differentiates a given DiffMaker object into NbDiffMaker or MyersDiffMaker. 
        /// </summary>
        /// <param name="diffMaker">DiffMaker object of a given diff.</param>
        private void DiffUpdate(DiffMaker diffMaker)
        {
            switch (diffMaker)
            {
                case NbDiffMaker nbDiffMaker:
                    DiffUpdate(nbDiffMaker);
                    break;
                case MyersDiffMaker myersDiffMaker:
                    DiffUpdate(myersDiffMaker);
                    break;
            }
        }
        
        /// <summary>
        /// Displays the output of NbDiffMaker.
        /// </summary>
        /// <param name="nbDiffMaker">NbDiffMaker with its diff computed.</param>
        private void DiffUpdate(NbDiffMaker nbDiffMaker)
        {
            _textBoxFileContent.Document.Blocks.Clear();

            var paragraph = new Paragraph();
            var sb = new StringBuilder();

            for (var i = 0; i < nbDiffMaker.FileBlocks[_diffFileAffiliation].Count; i++)
            {
                var block = nbDiffMaker.FileBlocks[_diffFileAffiliation][i];

                if (!Logging.DisplayRules[_diffFileAffiliation].Contains(block.Origin)) continue;

                if (Settings.Default.Setting_DisplayBlockHeaders)
                {
                    var text = $"[{block.Origin.ToString()}";
                    if (block.Origin == DiffOrigin.Moved) text += $" {block.Diff}";
                    text += "]";
                    if (i > 0)
                        text += ((StringChunk) nbDiffMaker.FileBlocks[_diffFileAffiliation][i - 1].ChunkMatches.Last()
                            .Usage[_diffFileAffiliation].Chunk).Suffix;
                    sb.Append(AddText(paragraph, text, Logging.BrushRules[DiffOrigin.None]));
                }

                foreach (var match in block.ChunkMatches)
                {
                    if (Settings.Default.Setting_DisplayChunkNumbers)
                    {
                        sb.Append(AddText(paragraph,
                            $"[{match.Usage[_diffFileAffiliation].ChunkIndex.ToString($"D{nbDiffMaker.ChunkNumberDigitsCount}")}]",
                            Logging.BrushRules[DiffOrigin.None]));
                    }

                    sb.Append(AddText(paragraph, match.Usage[_diffFileAffiliation].Chunk.ToString(),
                        Logging.BrushRules[block.Origin]));
                }
            }

            _textBoxFileContent.Document.PageWidth =
                PageWidth(new TextRange(paragraph.ContentStart, paragraph.ContentEnd).Text.Length,
                    sb.ToString().Count(c => c == '\n'));
            _textBoxFileContent.Document.Blocks.Add(paragraph);
        }

        /// <summary>
        /// Displays the output of MyersDiffMaker.
        /// </summary>
        /// <param name="myersDiffMaker">MyersDiffMaker with its diff calculated.</param>
        private void DiffUpdate(MyersDiffMaker myersDiffMaker)
        {
            _textBoxFileContent.Document.Blocks.Clear();

            var paragraph = new Paragraph();
            var sb = new StringBuilder();

            var chunkNum = 0;
            foreach (var chunkOriginInfo in myersDiffMaker.Result)
            {
                if (!Logging.DisplayRules[_diffFileAffiliation].Contains(chunkOriginInfo.Origin)) continue;

                var metaText =
                    $"{(Settings.Default.Setting_DisplayBlockHeaders ? Logging.MyersChunkOriginVisualisation[chunkOriginInfo.Origin] : "")}" +
                    $"{(Settings.Default.Setting_DisplayChunkNumbers ? chunkNum.ToString($"D{myersDiffMaker.ChunkNumberDigitsCount}") : "")}"
                        .Trim();

                if (metaText.Length > 0)
                    sb.Append(AddText(paragraph, $"[{metaText}]", Logging.BrushRules[DiffOrigin.None]));


                sb.Append(AddText(paragraph, chunkOriginInfo.Chunk.ToString(),
                    Logging.BrushRules[chunkOriginInfo.Origin]));

                chunkNum++;
            }

            _textBoxFileContent.Document.PageWidth =
                PageWidth(new TextRange(paragraph.ContentStart, paragraph.ContentEnd).Text.Length,
                    sb.ToString().Count(c => c == '\n'));
            _textBoxFileContent.Document.Blocks.Add(paragraph);
        }

        /// <summary>
        /// Estimates a recommended width of _textBoxFileContent to try to prevent text wrapping and resulting performance drops.  
        /// </summary>
        /// <param name="textLength">Length of the text which is to be displayed.</param>
        /// <param name="lineCount">Number of lines in the text.</param>
        /// <returns>The width in pixels.</returns>
        private double PageWidth(int textLength, int lineCount) => textLength * 5 * _textBoxFileContent.FontSize /
                                                                   (lineCount != 0 ? lineCount : 1);

        /// <summary>
        /// Adds a string with a given color into paragraph. 
        /// </summary>
        /// <param name="paragraph">The Paragraph object.</param>
        /// <param name="text">string to be added.</param>
        /// <param name="color">Desired color.</param>
        /// <returns>The added text.</returns>
        private static string AddText(Paragraph paragraph, string text, Brush color)
        {
            var run = new Run
            {
                Text = text,
                Foreground = color
            };

            paragraph.Inlines.Add(run);

            return text;
        }

        ~FileControlsSet()
        {
            _mainWindow.OnDiffUpdate -= DiffUpdate;
        }
    }
}