﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using diff_main.Properties;
using DiffAPI;
using DiffMyers;
using Microsoft.Win32;
using NBDiff;

namespace diff_main
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        /// <summary>
        /// Encoding to be used during file loading and saving.
        /// </summary>
        private Encoding FileEncoding => Encoding.GetEncoding(Settings.Default.Setting_Encoding);

        /// <summary>
        /// Text splitter to be used to create inputs for diff algorithms.
        /// </summary>
        private TextSplitter CurrentTextSplitter => TextSplitters[Settings.Default.Setting_TextSplitter];

        /// <summary>
        /// Diff maker to be used.
        /// </summary>
        private DiffMaker CurrentDiffMaker => GetDiffMaker(Settings.Default.Setting_DiffAlgorithm);

        /// <summary>
        /// Called when a progress within diff creation is made.
        /// </summary>
        public event Action<DiffMaker> OnDiffUpdate;

        /// <summary>
        /// Called when user settings are updated.
        /// </summary>
        public event Action OnSettingsUpdate;

        /// <summary>
        /// Current diff.
        /// </summary>
        private DiffMaker Diff
        {
            get => _diff;
            set
            {
                ButtonLog.IsEnabled = value != null;
                _diff = value;
            }
        }

        /// <summary>
        /// Holds the value for Diff.
        /// </summary>
        private DiffMaker _diff;

        /// <summary>
        /// FileControlsSets present in MainWindow.
        /// </summary>
        private readonly Dictionary<DiffFile, FileControlsSet> _fileControlsSets;

        /// <summary>
        /// Currently opened _settingsWindow.
        /// </summary>
        private SettingsWindow _settingsWindow;

        /// <summary>
        /// Relations of TextSplitterOptions (used in settings) and pre-defined instances of TextSplitter.
        /// </summary>
        private static readonly Dictionary<TextSplitterOption, TextSplitter> TextSplitters =
            new Dictionary<TextSplitterOption, TextSplitter>
            {
                {TextSplitterOption.Code, TextSplitter.Code},
                {TextSplitterOption.Line, TextSplitter.NewLine},
                {TextSplitterOption.SimpleLine, TextSplitter.SimpleLine},
                {TextSplitterOption.SimpleWhitespace, TextSplitter.SimpleWhitespace}
            };

        /// <summary>
        /// Relations of DiffAlgorithm (used in settings) and DiffMaker classes.
        /// </summary>
        /// <param name="algorithm">DiffAlgorithm value.</param>
        /// <returns>Appropriate DiffMaker.</returns>
        /// <exception cref="ArgumentOutOfRangeException">Occurs when the DiffAlgorithm supplied doesn't correspond to any class.</exception>
        private DiffMaker GetDiffMaker(DiffAlgorithm algorithm)
        {
            switch (algorithm)
            {
                case DiffAlgorithm.Myers: return new MyersDiffMaker(Settings.Default.Setting_MyersAlgorithmMaxESLength);
                case DiffAlgorithm.NbDiff: return new NbDiffMaker();
                default:
                    throw new ArgumentOutOfRangeException(nameof(algorithm), algorithm, null);
            }
        }

        /// <summary>
        /// Constructor. Initializes _fileControlsSets and creates logic to allow/disallow use of the Log button.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            _fileControlsSets = new Dictionary<DiffFile, FileControlsSet>
            {
                {
                    DiffFile.Original,
                    new FileControlsSet(DiffFile.Original, this, ButtonOpenOrigin, TextBoxOrigin, LabelOriginFileName)
                },
                {
                    DiffFile.New,
                    new FileControlsSet(DiffFile.New, this, ButtonOpenNew, TextBoxNew, LabelNewFileName)
                }
            };

            OnDiffUpdate += diff => ButtonLog.IsEnabled = diff != null;
            OnSettingsUpdate += UpdateDiff;
        }

        /// <summary>
        /// Called upon the use of the left Open File button.
        /// </summary>
        private void ButtonOpenOrigin_OnClick(object sender, RoutedEventArgs e)
        {
            LoadFile(DiffFile.Original, SelectFile);
        }

        /// <summary>
        /// Called upon the use of the right Open File button.
        /// </summary>
        private void ButtonOpenNew_OnClick(object sender, RoutedEventArgs e)
        {
            LoadFile(DiffFile.New, SelectFile);
        }

        /// <summary>
        /// Called upon the use of the Settings button. Initializes a new SettingsWindow or creates focus on the currently opened one.
        /// </summary>
        private void ButtonSettings_OnClick(object sender, RoutedEventArgs e)
        {
            if (_settingsWindow != null) _settingsWindow.Focus();
            else
            {
                _settingsWindow = new SettingsWindow();
                _settingsWindow.Save += OnSettingsUpdate;
                _settingsWindow.Closed += (o, args) => _settingsWindow = null;
                _settingsWindow.Show();
            }
        }

        /// <summary>
        /// Launches an OpenFileDialog.
        /// </summary>
        /// <param name="diffFile">Diff file characteristic of the file being opened.</param>
        /// <param name="onDone">Called on the OpenFileDialog.FileOk event.</param>
        private void LoadFile(DiffFile diffFile, CancelEventHandler onDone = null)
        {
            var openFileDialog = new OpenFileDialog()
            {
                CheckFileExists = true,
                CheckPathExists = true,
                Title = "Open File",
                Tag = diffFile
            };
            openFileDialog.FileOk += onDone;

            openFileDialog.ShowDialog();
        }

        /// <summary>
        /// Used when a file is selected by OpenFileDialog.
        /// </summary>
        /// <param name="sender">OpenFileDialog.</param>
        /// <param name="args">Arguments.</param>
        private void SelectFile(object sender, CancelEventArgs args)
        {
            if (!(sender is OpenFileDialog openFileDialog)) return;
            if (!(openFileDialog.Tag is DiffFile diffFile)) return;

            _fileControlsSets[diffFile].SelectedFile = openFileDialog.FileName;

            UpdateDiff();
        }

        /// <summary>
        /// Checks whether a new diff can be created and if so, creates the new diff.
        /// </summary>
        private void UpdateDiff()
        {
            if (_fileControlsSets[DiffFile.Original].SelectedFile == null ||
                _fileControlsSets[DiffFile.New].SelectedFile == null)
            {
                foreach (var set in _fileControlsSets.Values)
                    set.SimpleDisplay();
                return;
            }

            var c1 = CurrentTextSplitter
                .Split(File.ReadAllText(_fileControlsSets[DiffFile.Original].SelectedFile, FileEncoding))
                .ToArray() as IChunk[];
            var c2 =
                CurrentTextSplitter.Split(File.ReadAllText(_fileControlsSets[DiffFile.New].SelectedFile, FileEncoding))
                    .ToArray() as IChunk[];
            var newDiff = CurrentDiffMaker.AssignSequences(c1, c2);
            newDiff.OnStatusUpdate += (status, message) =>
            {
                Dispatcher.Invoke(() =>
                {
                    LabelStatus.Content = $"{status.ToString()}. {message}";
                    switch (status)
                    {
                        case DiffStatus.Done:
                            Diff = newDiff;
                            OnDiffUpdate?.Invoke(Diff);
                            GC.Collect();
                            break;
                        case DiffStatus.Failed:
                            Diff = null;
                            foreach (var fileControlsSet in _fileControlsSets.Values)
                                fileControlsSet.SimpleDisplay();
                            break;
                    }
                });
            };
            new Thread(() => newDiff.Compute()).Start();
        }

        /// <summary>
        /// Logs the output of the current Diff.
        /// </summary>
        /// <param name="stream">Stream to be logged into.</param>
        private void Log(Stream stream)
        {
            switch (Diff)
            {
                case NbDiffMaker nbDiffMaker:
                    Log(stream, nbDiffMaker);
                    break;
                case MyersDiffMaker myersDiffMaker:
                    Log(stream, myersDiffMaker);
                    break;
            }
        }

        /// <summary>
        /// Logs the output of an NbDiffMaker.
        /// </summary>
        /// <param name="stream">Stream to be logged into.</param>
        /// <param name="nbDiff">NbDiffMaker instance.</param>
        private void Log(Stream stream, NbDiffMaker nbDiff)
        {
            if (Diff == null) return;

            var log = new StreamWriter(stream, FileEncoding);

            for (var i = 0; i < nbDiff.BlocksAll.Count; i++)
            {
                var block = nbDiff.BlocksAll[i];

                if (Settings.Default.Setting_DisplayBlockHeaders)
                {
                    log.Write($"[{block.Origin + (block.Diff != null ? $" {block.Diff}" : "")}]");
                    if (i > 0)
                    {
                        var lastChunkMatch = nbDiff.BlocksAll[i - 1].ChunkMatches.Last();
                        log.Write((lastChunkMatch.Usage[DiffFile.New]?.Chunk.ToString().Last() ??
                                   lastChunkMatch.Usage[DiffFile.Original].Chunk.ToString().Last()) == '\n'
                            ? "\n"
                            : " ");
                    }
                }

                foreach (var match in block.ChunkMatches)
                {
                    if (Settings.Default.Setting_DisplayChunkNumbers)
                    {
                        log.Write("[");
                        log.Write(match.Usage[DiffFile.Original] != null
                            ? match.Usage[DiffFile.Original].ChunkIndex.ToString($"D{nbDiff.ChunkNumberDigitsCount}")
                            : new string('_', nbDiff.ChunkNumberDigitsCount));
                        log.Write("->");
                        log.Write(match.Usage[DiffFile.New] != null
                            ? match.Usage[DiffFile.New].ChunkIndex.ToString($"D{nbDiff.ChunkNumberDigitsCount}")
                            : new string('_', nbDiff.ChunkNumberDigitsCount));
                        log.Write("]");
                    }

                    log.Write(match.Usage[DiffFile.New] != null
                        ? match.Usage[DiffFile.New].Chunk
                        : match.Usage[DiffFile.Original].Chunk);
                }
            }

            log.Close();
        }

        /// <summary>
        /// Logs the output of an MyersDiffMaker.
        /// </summary>
        /// <param name="stream">Stream to be logged into.</param>
        /// <param name="myersDiff">MyersDiffMaker instance.</param>
        private void Log(Stream stream, MyersDiffMaker myersDiff)
        {
            if (Diff == null) return;

            var log = new StreamWriter(stream);
            var f1ChunkNo = 0;
            var f2ChunkNo = 0;
            foreach (var chunkOrigin in myersDiff.Result)
            {
                var metaText = "";
                if (Settings.Default.Setting_DisplayBlockHeaders)
                    metaText += Logging.MyersChunkOriginVisualisation[chunkOrigin.Origin];
                if (Settings.Default.Setting_DisplayChunkNumbers)
                    metaText +=
                        (Logging.DisplayRules[DiffFile.New].Contains(chunkOrigin.Origin) ? f2ChunkNo : f1ChunkNo)
                        .ToString($"D{myersDiff.ChunkNumberDigitsCount}");

                if (metaText.Length > 0)
                    log.Write($"[{metaText}]");

                log.Write(chunkOrigin.Chunk.ToString());

                if (Logging.DisplayRules[DiffFile.Original].Contains(chunkOrigin.Origin)) f1ChunkNo++;
                if (Logging.DisplayRules[DiffFile.New].Contains(chunkOrigin.Origin)) f2ChunkNo++;
            }

            log.Close();
        }

        /// <summary>
        /// Called upon a use of the Log button. Initializes a new SaveFileDialog.
        /// </summary>
        private void ButtonLog_OnClick(object sender, RoutedEventArgs e)
        {
            if (Diff == null) return;

            var saveFileDialog = new SaveFileDialog()
            {
                CheckPathExists = true,
                Title = "Save Log",
                Filter = "All Files (*.*)|*.*|Text Files (*.txt)|*.txt|Log Files (*.log)|*.log",
                FilterIndex = 2,
                FileName = "diff_log"
            };

            saveFileDialog.FileOk += SaveLogFileDialogOnFileOk;
            saveFileDialog.ShowDialog();
        }

        /// <summary>
        /// Called when a target log file is selected using a SaveFileDialog called by ButtonLog_OnClick. 
        /// </summary>
        private void SaveLogFileDialogOnFileOk(object sender, CancelEventArgs e)
        {
            if (!(sender is SaveFileDialog dialog)) return;

            using (var fs = dialog.OpenFile())
            {
                Log(fs);
                fs.Close();
            }
        }
    }

    /// <summary>
    /// TextSplitters to be displayed in user settings.
    /// </summary>
    [Serializable]
    internal enum TextSplitterOption
    {
        Line,
        Code,
        SimpleLine,
        SimpleWhitespace
    }

    /// <summary>
    /// Diff algorithms to be displayed in user settings. 
    /// </summary>
    [Serializable]
    internal enum DiffAlgorithm
    {
        Myers,
        NbDiff
    }
}