﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using diff_main.Properties;

namespace diff_main
{
    /// <summary>
    /// Interaction logic for SettingsWindow.xaml
    /// </summary>
    public partial class SettingsWindow
    {
        /// <summary>
        /// Called when the settings are saved.
        /// </summary>
        public event Action Save;

        /// <summary>
        /// Constructor. Prepares and fills values of controls present in window layout.
        /// </summary>
        public SettingsWindow()
        {
            InitializeComponent();

            FillCheckBox(CheckBoxBlockHeaders, Settings.Default.Setting_DisplayBlockHeaders,
                b => Settings.Default.Setting_DisplayBlockHeaders = b);

            FillCheckBox(CheckBoxChunkNumbers, Settings.Default.Setting_DisplayChunkNumbers,
                b => Settings.Default.Setting_DisplayChunkNumbers = b);

            FillComboBox(
                ComboBoxDiffAlgorithm,
                Enum.GetNames(typeof(DiffAlgorithm)),
                (DiffAlgorithm[]) Enum.GetValues(typeof(DiffAlgorithm)),
                Settings.Default.Setting_DiffAlgorithm,
                selection => Settings.Default.Setting_DiffAlgorithm =
                    selection is DiffAlgorithm option ? option : DiffAlgorithm.Myers
            );

            NumericTextBox(TextBoxMyersMaxEsl);
            TextBoxMyersMaxEsl.Text = Settings.Default.Setting_MyersAlgorithmMaxESLength.ToString();
            TextBoxMyersMaxEsl.TextChanged += (sender, args) =>
            {
                if (int.TryParse(TextBoxMyersMaxEsl.Text, out var val))
                    Settings.Default.Setting_MyersAlgorithmMaxESLength = val;
            };

            NumericDecimalTextBox(TextBoxFontSize);
            TextBoxFontSize.Text = Settings.Default.Setting_FontSize.ToString(CultureInfo.InvariantCulture);
            TextBoxFontSize.TextChanged += (sender, args) =>
            {
                if (double.TryParse(TextBoxFontSize.Text, NumberStyles.Any, CultureInfo.InvariantCulture, out var val))
                    Settings.Default.Setting_FontSize = val;
            };
            
            FillComboBox(
                ComboBoxTextSplitting,
                Enum.GetNames(typeof(TextSplitterOption)),
                (TextSplitterOption[]) Enum.GetValues(typeof(TextSplitterOption)),
                Settings.Default.Setting_TextSplitter,
                selection => Settings.Default.Setting_TextSplitter =
                    selection is TextSplitterOption option ? option : TextSplitterOption.Line
            );

            var encodings = Encoding.GetEncodings();
            FillComboBox(
                ComboBoxEncoding,
                encodings.Select(i => i.DisplayName).ToArray(),
                encodings.Select(i => i.CodePage).ToArray(),
                Settings.Default.Setting_Encoding,
                item =>
                {
                    if (item is int i) Settings.Default.Setting_Encoding = i;
                });
        }

        /// <summary>
        /// Prepares a CheckBox.
        /// </summary>
        /// <param name="checkBox">CheckBox.</param>
        /// <param name="value">Initial CheckBox value.</param>
        /// <param name="checked">Called when the value of checkBox is changed.</param>
        private static void FillCheckBox(CheckBox checkBox, bool value, Action<bool> @checked)
        {
            checkBox.IsChecked = value;
            if (@checked == null) return;

            var routedEventHandler = new RoutedEventHandler((sender, args) =>
            {
                var isChecked = ((CheckBox) sender).IsChecked;
                @checked(isChecked != null && (bool) isChecked);
            });
            checkBox.Checked += routedEventHandler;
            checkBox.Unchecked += routedEventHandler;
        }

        /// <summary>
        /// Fills a ComboBox with ComboBoxItems created from given parameters. Creates a SelectionChanged listener.
        /// </summary>
        /// <param name="comboBox">ComboBox.</param>
        /// <param name="values">ComboBoxItem values to be displayed to the user.</param>
        /// <param name="tags">Values to be worked with internally (are not displayed to the user).</param>
        /// <param name="selectedItem">An item (from tags) initially selected.</param>
        /// <param name="selectionChanged">Called when the selection is changed. Corresponding tag is passed as an argument.</param>
        /// <typeparam name="T">Type of tags.</typeparam>
        /// <exception cref="IndexOutOfRangeException">Thrown when the lengths of values and tags don't match.</exception>
        private static void FillComboBox<T>(ComboBox comboBox, string[] values, T[] tags, T selectedItem,
            Action<object> selectionChanged)
        {
            if (values.Length != tags.Length)
                throw new IndexOutOfRangeException("Lengths of values and tags don't match");

            for (var i = 0; i < values.Length; i++)
            {
                var item = new ComboBoxItem
                {
                    Content = values[i],
                    Tag = tags[i]
                };
                comboBox.Items.Add(item);

                if (Equals(tags[i], selectedItem)) comboBox.SelectedIndex = i;
            }

            if (selectionChanged != null)
                comboBox.SelectionChanged += (sender, args) =>
                    selectionChanged(((ComboBoxItem) comboBox.SelectedItem)?.Tag);
        }

        /// <summary>
        /// Makes a TextBox accept only numerical values.
        /// </summary>
        private static void NumericTextBox(TextBox textBox)
        {
            textBox.PreviewTextInput += NumericTextBoxOnPreviewTextInput;
        }
        
        /// <summary>
        /// Makes a TextBox accept only decimal values.
        /// </summary>
        private static void NumericDecimalTextBox(TextBox textBox)
        {
            textBox.PreviewTextInput += NumericDecimalTextBoxOnPreviewTextInput;
        }

        /// <summary>
        /// Checks whether values of TextBox are numeric.
        /// </summary>
        private static void NumericTextBoxOnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        /// <summary>
        /// Checks whether values of TextBox are decimal.
        /// </summary>
        private static void NumericDecimalTextBoxOnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var regex = new Regex(@"[^0-9\" + CultureInfo.InvariantCulture.NumberFormat.NumberDecimalSeparator + @"]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        /// <summary>
        /// Called upon a use of the Cancel button.
        /// </summary>
        private void ButtonCancel_OnClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Called upon a use of the OK button. Saves the settings in their current state.
        /// </summary>
        private void ButtonOk_OnClick(object sender, RoutedEventArgs e)
        {
            Settings.Default.Save();
            Save?.Invoke();
            Close();
        }

        /// <summary>
        /// Called when SettingsWindow is closing. Reverts the settings to the state in which they are currently saved.
        /// </summary>
        private void SettingsWindow_OnClosing(object sender, CancelEventArgs e)
        {
            Settings.Default.Reload();
        }
    }
}