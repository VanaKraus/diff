﻿using System.Collections.Generic;
using System.Windows.Media;
using DiffAPI;

namespace diff_main
{
    /// <summary>
    /// Contains rules for diff logging and displaying.
    /// </summary>
    public static class Logging
    {
        /// <summary>
        /// Which diff origins are relevant in relation to a certain diff file type.
        /// </summary>
        public static readonly Dictionary<DiffFile, DiffOrigin[]> DisplayRules = new Dictionary<DiffFile, DiffOrigin[]>
        {
            {DiffFile.Original, new[] {DiffOrigin.Moved, DiffOrigin.Removed}},
            {DiffFile.New, new[] {DiffOrigin.Moved, DiffOrigin.Added}}
        };

        /// <summary>
        /// Colors to be associated with certain diff origins.
        /// </summary>
        public static readonly Dictionary<DiffOrigin, Brush> BrushRules = new Dictionary<DiffOrigin, Brush>
        {
            {DiffOrigin.Moved, Brushes.Black},
            {DiffOrigin.Added, Brushes.Green},
            {DiffOrigin.Removed, Brushes.DarkRed},
            {DiffOrigin.None, Brushes.Gray}
        };

        /// <summary>
        /// How chunk origins should be labeled when using the Myers' algorithm.
        /// </summary>
        public static readonly Dictionary<DiffOrigin, string> MyersChunkOriginVisualisation =
            new Dictionary<DiffOrigin, string>
            {
                {DiffOrigin.Moved, " "}, {DiffOrigin.Added, "+"}, {DiffOrigin.Removed, "-"}
            };
    }
}