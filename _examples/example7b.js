function mFunc(i) {
    // an important function
    // does a lot of things
    console.log(i);
}

let i = 0;
while(true) {
    i++;
    mFunc(i);

    i++;
    if(i < 20) continue;

    mFunc(i);
}
