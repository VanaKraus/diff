﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;

namespace diff
{
    class Program
    {
        static string file1 = "tst1.txt";
        static string file2 = "tst2.txt";

        static void Main(string[] args)
        {
            LineInfo[] h1, h2;

            using (FileStream fs1 = File.Open(file1, FileMode.Open))
            {
                h1 = GetMD5Stream(fs1);
            }

            using (FileStream fs2 = File.Open(file2, FileMode.Open))
            {
                h2 = GetMD5Stream(fs2);
            }

            List<Comparator> comparators = new List<Comparator>();
            Dictionary<string, Comparator> compDict = new Dictionary<string, Comparator>();

            for (int i = 0; i < h1.Length; i++)
            {
                Comparator comparator = new Comparator() { lineInfo = h1[i], i1 = i, i2 = -1 };
                try
                {
                    comparators.Add(comparator);
                    compDict.Add(h1[i].hash, comparator);
                }
                catch (ArgumentException arge)
                {
                    Console.WriteLine($"{arge.Message} {i}");
                }
            }

            Dictionary<int, Comparator> unsortedFrom2 = new Dictionary<int, Comparator>();

            for (int i = 0; i < h2.Length; i++)
            {
                if (compDict.ContainsKey(h2[i].hash))
                {
                    var comp = compDict[h2[i].hash];
                    comp.i2 = i;
                }
                else
                {
                    Comparator comparator = new Comparator() { lineInfo = h2[i], i1 = -1, i2 = i };
                    compDict.Add(h2[i].hash, comparator);
                    //comparators.Add(comparator);
                    unsortedFrom2.Add(i, comparator);
                }
            }

            for(int i = comparators.Count - 1; i >= 0; i--)
            {
                Comparator comp = comparators[i];
                while(unsortedFrom2.ContainsKey(comp.i2 - 1))
                {
                    comparators.Insert(i++, comp = unsortedFrom2[comp.i2 - 1]);
                    unsortedFrom2.Remove(comp.i2 - 1);
                }
            }

            int? idiff = null;
            ChangeInfo ci = ChangeInfo.Block;
            foreach (var comp in comparators)
            {
                ChangeInfo nci = ChangeInfo.Block;

                int? nidiff = null;
                if (comp.i1 != -1 && comp.i2 != -1)
                {
                    nidiff = comp.i1 - comp.i2;
                }
                else if (comp.i1 == -1) nci = ChangeInfo.New;
                else if (comp.i2 == -1) nci = ChangeInfo.Removed;

                if (nci != ci || (nci == ChangeInfo.Block && nidiff != idiff))
                    Console.WriteLine("-----------------------------------");

                ci = nci;
                idiff = nidiff;

                Console.WriteLine($"{comp.lineInfo.hash}\t{ci}\t{comp.i1}\t{comp.i2}\t{comp.lineInfo.line}");
            }

            Console.ReadKey();
        }

        static LineInfo[] GetMD5Stream(FileStream file)
        {
            var md5 = MD5.Create();

            StreamReader streamReader = new StreamReader(file);

            List<LineInfo> hashes = new List<LineInfo>();

            while (!streamReader.EndOfStream)
            {
                string line = streamReader.ReadLine();
                string hash = MD5ToString(md5.ComputeHash(Encoding.ASCII.GetBytes(line)));
                hashes.Add(new LineInfo { line = line, hash = hash });
            }

            return hashes.ToArray();
        }

        static string MD5ToString(byte[] md5bytes)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < md5bytes.Length; i++)
            {
                sb.Append(md5bytes[i].ToString("X2"));
            }
            return sb.ToString();

        }
    }

    class Comparator
    {
        public LineInfo lineInfo;
        public int i1, i2;
    }

    struct LineInfo
    {
        public string hash, line;
    }

    enum ChangeInfo { Block, Removed, New }

}
