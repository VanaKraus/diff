﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;

namespace DiffAPI
{
    public class Diff
    {
        public readonly string File1, File2;
        public readonly TextSplitter TextSplitter;
        private readonly Encoding _file1Encoding;
        private readonly Encoding _file2Encoding;

        public ReadOnlyCollection<ChunkBlock> BlocksAll { get; private set; }
        public ReadOnlyDictionary<DiffFile, ReadOnlyCollection<ChunkBlock>> FileBlocks { get; private set; }
        public ReadOnlyDictionary<DiffFile, ReadOnlyCollection<ChunkUsage>> FileChunkUsages { get; private set; }

        public int ChunkUsagesCount => FileChunkUsages.Values.Select(value => value.Count).Max();
        public int ChunkNumberDigitsCount => Convert.ToInt32(Math.Ceiling(Math.Log10(ChunkUsagesCount)));

        public event DiffStatusUpdate OnStatusUpdate;

        private Dictionary<int, ChunkRegister> _chunkRegisters;
        private List<ChunkMatch> _matches;
        private ChunkUsage[] _chunkUsages1, _chunkUsages2;

        public Diff(string file1, string file2, TextSplitter textSplitter,
            Encoding file1Encoding = null, Encoding file2Encoding = null)
        {
            File1 = file1;
            File2 = file2;
            TextSplitter = textSplitter;
            _file1Encoding = file1Encoding ?? Encoding.UTF8;
            _file2Encoding = file2Encoding ?? Encoding.UTF8;
        }

        public Diff Compute()
        {
            OnStatusUpdate?.Invoke(DiffStatus.Loading);

            var l1Text = File.ReadAllText(File1);
            var l2Text = File.ReadAllText(File2);

            OnStatusUpdate?.Invoke(DiffStatus.Preparing);
            var l1S = TextSplitter.Split(l1Text);
            var l2S = TextSplitter.Split(l2Text);

            OnStatusUpdate?.Invoke(DiffStatus.Matching);
            _chunkRegisters =
                CreateChunkRegisters(l1S.ToArray(), l2S.ToArray(), out _chunkUsages1, out _chunkUsages2); 
            _matches = CreateMatches(_chunkRegisters, _chunkUsages1, _chunkUsages2); 

            // block corrections
            OnStatusUpdate?.Invoke(DiffStatus.Correcting);
            _matches = SealHoles(_matches, _chunkUsages1); // tc linear, mc constant
            var blocks = CreateBlocks(_matches); // tc, mc linear

            OnStatusUpdate?.Invoke(DiffStatus.Compiling);
            BlocksAll = Array.AsReadOnly(blocks.Where(block => block.Origin != DiffOrigin.None).ToArray());

            FileBlocks = new ReadOnlyDictionary<DiffFile, ReadOnlyCollection<ChunkBlock>>(
                new Dictionary<DiffFile, ReadOnlyCollection<ChunkBlock>>
                {
                    {DiffFile.Original, Array.AsReadOnly(ChunkBlocksFromChunkUsages(_chunkUsages1))},
                    {DiffFile.New, Array.AsReadOnly(ChunkBlocksFromChunkUsages(_chunkUsages2))},
                });

            FileChunkUsages = new ReadOnlyDictionary<DiffFile, ReadOnlyCollection<ChunkUsage>>(
                new Dictionary<DiffFile, ReadOnlyCollection<ChunkUsage>>
                {
                    {DiffFile.Original, Array.AsReadOnly(_chunkUsages1)},
                    {DiffFile.New, Array.AsReadOnly(_chunkUsages2)}
                });

            OnStatusUpdate?.Invoke(DiffStatus.Done);
            return this;
        }

        private static Dictionary<int, ChunkRegister> CreateChunkRegisters(Chunk[] file1Chunks, Chunk[] file2Chunks,
            out ChunkUsage[] file1ChunkUsages, out ChunkUsage[] file2ChunkUsages)
        {
            var registers = new Dictionary<int, ChunkRegister>();
            
            file1ChunkUsages = RegisterChunks(registers, file1Chunks, DiffFile.Original);
            file2ChunkUsages = RegisterChunks(registers, file2Chunks, DiffFile.New);

            return registers;
        }

        private static List<ChunkMatch> CreateMatches(Dictionary<int, ChunkRegister> chunkRegisters,
            ChunkUsage[] file1ChunkUsages, ChunkUsage[] file2ChunkUsages)
        {
            List<ChunkMatch> matches = new List<ChunkMatch>();
            
            // Use a greedy chunk match in every register
            foreach (var register in chunkRegisters.Values)
            {
                register.GreedyChunkMatch(true);
            }
            
            // Go through all chunks removed from the beginning
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var lu in file1ChunkUsages)
            {
                if (lu.Match.Usage[DiffFile.New] != null) break;
                matches.Add(lu.Match);
            }

            // Go through all chunks from the new file
            foreach (var chunkUsage2 in file2ChunkUsages)
            {
                matches.Add(chunkUsage2.Match);
                
                var chunkUsage1 = chunkUsage2.Match.Usage[DiffFile.Original];
                if (chunkUsage1 == null) continue;
                
                // Add all removed chunks directly following the current chunkUsage2
                while (chunkUsage1.ChunkIndex < file1ChunkUsages.Length - 1)
                {
                    chunkUsage1 = file1ChunkUsages[chunkUsage1.ChunkIndex + 1];
                    if (chunkUsage1.Match.Origin != DiffOrigin.Removed) break;
                    matches.Add(chunkUsage1.Match);
                }
            }

            return matches;
        }

        private static IEnumerable<ChunkBlock> CreateBlocks(List<ChunkMatch> matches)
        {
            var blocks = new List<ChunkBlock>();
            var currentBlockMatches = new List<ChunkMatch>();

            int? diff = null; // difference between the old and the new chunk index
            for (var i = 0; i < matches.Count; i++)
            {
                var match = matches[i];

                if (match.Diff != diff || (i > 0 && matches[i].Origin != matches[i - 1].Origin))
                {
                    blocks.Add(new ChunkBlock(currentBlockMatches));
                    currentBlockMatches = new List<ChunkMatch>();
                }

                currentBlockMatches.Add(matches[i]);
                diff = match.Diff;
            }

            blocks.Add(new ChunkBlock(currentBlockMatches));

            return blocks;
        }

        private static ChunkBlock[] ChunkBlocksFromChunkUsages(IEnumerable<ChunkUsage> chunkUsages)
        {
            var chunkBlocks = new List<ChunkBlock>();
            foreach (var usage in chunkUsages)
            {
                if (usage.Match?.Block == null)
                    throw new DiffException("Chunk usage not matched or not present in a block");

                var block = usage.Match.Block;
                if(chunkBlocks.Count == 0 || !block.Equals(chunkBlocks[chunkBlocks.Count - 1]))
                    chunkBlocks.Add(block);
            }

            return chunkBlocks.ToArray();
        }

        private static List<ChunkMatch> SealHoles(List<ChunkMatch> matches, ChunkUsage[] file1ChunkUsages)
        {
            matches = new List<ChunkMatch>(matches);
            int? diff = null; // difference between the old and the new chunk index
            for (var i = 0; i < matches.Count; i++)
            {
                // attempts to seal "holes" caused by greedy chunk-matching (e.g. a misplaced origin of a blank chunk)
                if (diff != null && diff.Value != matches[i].Diff)
                    SealBlockHole(matches, i, (int) diff, file1ChunkUsages);

                diff = matches[i].Diff;
            }

            return matches;
        }

        private static bool SealBlockHole(List<ChunkMatch> matches, int matchIndex, int previousDiff,
            ChunkUsage[] file1ChunkUsages)
        {
            var match = matches[matchIndex];
            
            var cu1 = match.Usage[DiffFile.Original];
            var cu2 = match.Usage[DiffFile.New];

            if (cu2 == null) return false;

            // tries to find a chunk (parallelCu1) from the old file which should have been matched with cu2
            // provided that it belongs to the same block (as cu2)
            var parallelCu1Index = cu2.ChunkIndex - previousDiff;

            if (parallelCu1Index < 0 || parallelCu1Index >= file1ChunkUsages.Length) // if out of range
                return false;

            var parallelCu1 = file1ChunkUsages[parallelCu1Index];

            // checks if parallelLu1 really could belong to the same block as lu2
            if (!parallelCu1.Register.Equals(match.ChunkRegister) || parallelCu1.Match.Equals(match)) return false;

            // destroy old matches, create new ones
            var altCu2 = parallelCu1.Match.Usage[DiffFile.New];
            var altCu2MatchIndex = matches.IndexOf(parallelCu1.Match);
            var register = match.ChunkRegister;

            parallelCu1.Match.Destroy();
            match.Destroy();

            var newDesiredMatch = register.RegisterMatch(parallelCu1, cu2);
            var newAltMatch = register.RegisterMatch(cu1, altCu2);

            if (matchIndex < altCu2MatchIndex) // current match came first
            {
                matches.RemoveAt(altCu2MatchIndex);
                matches.RemoveAt(matchIndex);

                matches.Insert(matchIndex, newDesiredMatch);
                matches.Insert(altCu2MatchIndex, newAltMatch);
            }
            else // alternative match came first
            {
                matches.RemoveAt(matchIndex);
                matches.RemoveAt(altCu2MatchIndex);

                matches.Insert(altCu2MatchIndex, newAltMatch);
                matches.Insert(matchIndex, newDesiredMatch);
            }

            return true;
        }

        private static ChunkUsage[] RegisterChunks(Dictionary<int, ChunkRegister> registers, Chunk[] chunks, DiffFile diffFile)
        {
            var usages = new ChunkUsage[chunks.Length];

            for (var i = 0; i < chunks.Length; i++)
            {
                var chunk = chunks[i];
                var hash = chunk.Content.GetHashCode();
                ChunkRegister register;

                if (!registers.ContainsKey(hash))
                {
                    register = new ChunkRegister(chunk.Content);
                    registers.Add(hash, register);
                }
                else
                    register = registers[hash];

                usages[i] = register.AddChunkUsage(diffFile, chunk, i);
            }

            return usages;
        }
    }
    
    public enum DiffStatus { None, Loading, Preparing, Matching, Correcting, Compiling, Done }

    public delegate void DiffStatusUpdate(DiffStatus status, string message = "");
}