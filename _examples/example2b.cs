﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;

namespace diff
{
    class Program
    {
        static string file1 = "tst1.txt";
        static string file2 = "tst2.txt";

        static void Main(string[] args)
        {
            LineInfo[] h1, h2;

            using (FileStream fs1 = File.Open(file1, FileMode.Open))
            {
                h1 = GetMD5Stream(fs1);
            }

            using (FileStream fs2 = File.Open(file2, FileMode.Open))
            {
                h2 = GetMD5Stream(fs2);
            }

            List<Comparator> comparators = new List<Comparator>();
            Dictionary<string, List<Comparator>> compDict = new Dictionary<string, List<Comparator>>();

            for (int i = 0; i < h1.Length; i++)
            {
                Comparator comparator = new Comparator(h1[i], i, -1);
                comparators.Add(comparator);

                if (!compDict.ContainsKey(h1[i].hash))
                    compDict.Add(h1[i].hash, new List<Comparator>() { comparator });
                else compDict[h1[i].hash].Add(comparator);

            }

            Dictionary<int, Comparator> unsortedFrom2 = new Dictionary<int, Comparator>();

            for (int i = 0; i < h2.Length; i++)
            {

                if (compDict.ContainsKey(h2[i].hash))
                {
                    bool assigned = false;
                    foreach (var comp in compDict[h2[i].hash])
                    {
                        if (comp.I2 == -1)
                        {
                            comp.I2 = i;
                            assigned = true;
                            break;
                        }
                    }

                    if(!assigned)
                    {
                        Comparator comparator = new Comparator(h2[i], -1, i);
                        compDict[h2[i].hash].Add(comparator);
                        unsortedFrom2.Add(i, comparator);
                    }
                }

                else
                {
                    Comparator comparator = new Comparator(h2[i], -1, i);
                    compDict.Add(h2[i].hash, new List<Comparator> { comparator });
                    unsortedFrom2.Add(i, comparator);
                }
            }

            for(int i = comparators.Count - 1; i >= 0; i--)
            {
                Comparator comp = comparators[i];
                while(unsortedFrom2.ContainsKey(comp.I2 - 1))
                {
                    comparators.Insert(i++, comp = unsortedFrom2[comp.I2 - 1]);
                    unsortedFrom2.Remove(comp.I2 - 1);
                }
            }

            var keyvals = unsortedFrom2.ToArray();
            foreach(var keyval in keyvals)
            {
                comparators.Add(keyval.Value);
                unsortedFrom2.Remove(keyval.Key);
            }

            int? idiff = null;
            ChangeInfo ci = ChangeInfo.Block;
            foreach (var comp in comparators)
            {
                ChangeInfo nci = ChangeInfo.Block;

                int? nidiff = null;
                if (comp.I1 != -1 && comp.I2 != -1)
                {
                    nidiff = comp.I1 - comp.I2;
                }
                else if (comp.I1 == -1) nci = ChangeInfo.New;
                else if (comp.I2 == -1) nci = ChangeInfo.Removed;

                if (nci != ci || (nci == ChangeInfo.Block && nidiff != idiff))
                    Console.WriteLine("-----------------------------------");

                ci = nci;
                idiff = nidiff;

                Console.WriteLine($"{comp.lineInfo.hash}\t{ci}\t{comp.I1}\t{comp.I2}\t{comp.lineInfo.line}");
            }

            Console.ReadKey();
        }

        static LineInfo[] GetMD5Stream(FileStream file)
        {
            var md5 = MD5.Create();

            StreamReader streamReader = new StreamReader(file);

            List<LineInfo> hashes = new List<LineInfo>();

            while (!streamReader.EndOfStream)
            {
                string line = streamReader.ReadLine();
                string hash = MD5ToString(md5.ComputeHash(Encoding.ASCII.GetBytes(line)));
                hashes.Add(new LineInfo { line = line, hash = hash });
            }

            return hashes.ToArray();
        }

        static string MD5ToString(byte[] md5bytes)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < md5bytes.Length; i++)
            {
                sb.Append(md5bytes[i].ToString("X2"));
            }
            return sb.ToString();

        }
    }

    class Comparator
    {
        public LineInfo lineInfo;
        private int i1, i2;
        public int I1
        {
            get => i1;
            set
            {
                if (i1 == -1) i1 = value;
                else throw new Exception();
            }
        }
        public int I2
        {
            get => i2;
            set
            {
                if (i2 == -1) i2 = value;
                else throw new Exception();
            }
        }


        public Comparator(LineInfo lineInfo, int i1, int i2)
        {
            this.lineInfo = lineInfo;
            this.i1 = i1;
            this.i2 = i2;
        }
    }

    struct LineInfo
    {
        public string hash, line;
    }

    enum ChangeInfo { Block, Removed, New }

}
